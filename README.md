# Indra

Projeto Devops - Indracompany.

Dados do ambiente de testes: CentOS Linux release 8.3.2011 - Docker version 20.10.1, build 831ebea - docker-compose version 1.23.2  - build 1110ad01

Estrutura de arquivos:
	
	indra-app # pasta raiz 

		www			# ponto de montagem servidor web.
		Mysql			# ponto de montagem banco de dados.
		apache-php		# pasta Dockerfile.
		docker-compose		# arquivos de deploy do projeto.	

Instruções para execução:
Entrar na pasta raiz do projeto onde está o aquivos docker-compose.yml e executar o comado: “Docker-compose up -d”

Dados da Aplicação:
Contêiner:
	indra-app_web_1
	db
Imagens:
    indra-app_web
    mysql


Jorge Brandão Junior 27/01/2021


